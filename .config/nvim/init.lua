require('plugins')
require('options')
require('treesitter-config')
require('lualine-config')
require('bufferline-config')
require('autopairs-config')
require('keybindings')
require('nvim-tree-config')
require('whichkey-config')
require('telescope-config')
require('colorizer-config')
require('lsp')
require('blankline-config')
require('format-config')
require('dashboard-config')
require('toggleterm-config')
--SET COLOR SCHEME TO GRUVBOX START
vim.opt.termguicolors = true
vim.o.background = "dark" -- or "light" for light mode
vim.cmd([[colorscheme gruvbox]])
--COLOR SCHEME END

